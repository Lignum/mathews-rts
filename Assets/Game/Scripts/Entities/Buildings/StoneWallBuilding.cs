﻿using RTS.Buildings;
using RTS.Game;
using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MathiewsRTS.Entities.Buildings {
    public class StoneWallBuilding : RTSBuilding {

        [SerializeField]
        MapTilePlataform tilePlataform = null;

        protected override void Awake() {
            base.Awake();

            addOnPlaced(() => checkWalls(true));
            addOnPlaced(createTiles);

        }

        
        void checkWalls(bool spand) {

            StoneWallBuilding stoneWallb;
            Vector3Int pos;

            HashSet<StoneWallBuilding> updated = new HashSet<StoneWallBuilding>();

            foreach(Transform wall in Body.GetChild(0)) {

                pos = RTSGameCtrl.worldToTilePosition(wall.position);
                stoneWallb = RTSTileMng.getTileObjAt<StoneWallBuilding>(pos);

                wall.gameObject.SetActive(stoneWallb == null);

                if(spand &&stoneWallb != null) {
                    updated.Add(stoneWallb);
                }
            }

            if(spand) {
                foreach(var item in updated) {
                    item.checkWalls(false);
                }
            }
        }

        void createTiles() {
            tilePlataform.createTiles(WorldPosition, TiledRotation);

        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            Gizmos.color = Color.red;

            if(Body != null) {
                tilePlataform.drawGizmos(Position, TiledObj.quaternionToTiledRotation(Body.rotation));
            } else {
                tilePlataform.drawGizmos(Position, TiledObj.quaternionToTiledRotation(transform.rotation));
            }
        }

    }
}