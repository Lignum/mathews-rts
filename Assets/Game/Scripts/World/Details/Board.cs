﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MathiewsRTS.World.Details {

    //[ExecuteInEditMode]
    public class Board : MonoBehaviour {

        [SerializeField]
        MeshRenderer _boardPlane;

        [SerializeField]
        MeshRenderer[] _boardSide;

        [SerializeField]
        MeshRenderer[] _boardCorner;

        private void Start() {
            createBoard();

        }

        [ContextMenu("Create Board")]
        public void createBoard() {

            Vector3 size = RTS.MapAI.MapCtr.WorldMapSize;

            transform.position = new Vector3(size.x / 2f, 0, size.z / 2f);
            transform.localScale = Vector3.one;

            createPlane(size);
            createSides(size);
            createCorners(size);
        }

        /*private void Update() {

            Vector3Int size = RTS.NavMeshMng.NavMeshCtr.getInstance().getMapSize();

            transform.position = new Vector3(size.x / 2f, 0, size.z / 2f);
            transform.localScale = Vector3.one;

            createPlane(size);
            createSides(size);
            createCorners(size);

        }*/

        void createPlane(Vector3 size) {

            _boardPlane.transform.localScale = new Vector3(size.x, 1, size.z);
            _boardPlane.transform.localPosition = Vector3.zero;

            _boardPlane.sharedMaterial.SetTextureScale("_BaseMap", new Vector2(size.x, size.z));

        }

        void createSides(Vector3 size) {

            _boardSide[0].transform.localScale = new Vector3(1, 1, size.z);
            _boardSide[0].transform.localPosition = new Vector3(-size.x / 2f, 0, 0);

            _boardSide[1].transform.localScale = new Vector3(1, 1, size.x);
            _boardSide[1].transform.localPosition = new Vector3(0, 0, size.z / 2f);

            _boardSide[2].transform.localScale = new Vector3(1, 1, size.z);
            _boardSide[2].transform.localPosition = new Vector3(size.x / 2f, 0, 0);

            _boardSide[3].transform.localScale = new Vector3(1, 1, size.x);
            _boardSide[3].transform.localPosition = new Vector3(0, 0, -size.z / 2f);

            Transform sideT;
            for(int i = 0; i < _boardSide.Length; i++) {
                sideT = _boardSide[i].transform;
                sideT.localRotation = Quaternion.Euler(0, i * 90, 0);
                _boardSide[i].sharedMaterial.SetTextureScale("_BaseMap", new Vector2(sideT.localScale.x, sideT.localScale.z));
            }
            
        }

        void createCorners(Vector3 size) {

            _boardCorner[0].transform.localPosition = new Vector3(-size.x / 2f, 0, size.z / 2f);
            _boardCorner[0].transform.localRotation = Quaternion.Euler(0, 0, 0);

            _boardCorner[1].transform.localPosition = new Vector3(size.x / 2f, 0, size.z / 2f);
            _boardCorner[1].transform.localRotation = Quaternion.Euler(0, 90, 0);

            _boardCorner[2].transform.localPosition = new Vector3(size.x / 2f, 0, -size.z / 2f);
            _boardCorner[2].transform.localRotation = Quaternion.Euler(0, 180, 0);

            _boardCorner[3].transform.localPosition = new Vector3(-size.x / 2f, 0, -size.z / 2f);
            _boardCorner[3].transform.localRotation = Quaternion.Euler(0, 270, 0);
        }

    }
}